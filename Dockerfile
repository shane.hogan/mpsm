FROM centos:latest

USER root

RUN yum install nodejs -y
RUN yum install npm -y
RUN node --version
RUN npm --version
RUN yum install docker -y
RUN docker --version


WORKDIR /home
RUN mkdir frontend
RUN mkdir api

COPY ./frontend/package.json ./frontend
RUN npm i
COPY ./api/package.json ./api
RUN npm i
COPY ./api ./api
COPY ./frontend ./frontend

WORKDIR /home/frontend
RUN npm run build

WORKDIR /home/api
RUN npm run build

EXPOSE 8443
EXPOSE 3000
WORKDIR /home/api
ENTRYPOINT node ./dist/
