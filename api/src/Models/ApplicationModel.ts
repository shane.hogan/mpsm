import {Document, model, Schema} from 'mongoose';
// @ts-ignore
import * as encrypt from 'mongoose-encryption';
import IApplication from '../interfaces/IApplication';
import * as uuid from 'uuid/v4';
import * as dotenv from 'dotenv';
import * as mongoose from "mongoose";
dotenv.config();

export interface IApplicationModel extends IApplication, Document {
    _id: string
}

const encryptSecret =
  'Some Long sentence that I need to store in an envrionment variable so no one can esee it 2345678';
const encKey = process.env.DB_32BYTE_STRING;
const sigKey = process.env.DB_64BYTE_STRING;
console.log(`Encryption key: ${encKey}`);
console.log(`Signing Keey: ${sigKey}`);
const ApplicationSchema = new Schema(
  {
    userId: { type: String, required: true },
    app_name: { type: String, required: true },
    status: {
      type: String,
      enum: [
        'Starting',
        'Pulling',
        'Building & Testing',
        'Deploying',
        'Running',
        'Stopped',
        'Errored'
      ]
    },
    unique_name: { type: String, required: true },
    aws_region: { type: String, default: 'us-east-1' },
    aws_access_key: { type: String, required: true },
    aws_secret: { type: String, required: true },
    aws_account_id: { type: String, required: true },
    git_owner: { type: String, default: 'clearavenue' },
    git_branch: { type: String, default: 'master' },
    git_report: { type: String, default: 'helloworld' },
    git_oauth_token: {
      type: String,
      default: process.env.GIT_OAUTH_TOKEN
    },
    beanstalkUrl: { type: String }
  },
  {
    timestamps: {}
  }
);

ApplicationSchema.plugin(encrypt, {
   encryptionKey: encKey,
   signingKey: sigKey,
 excludeFromEncryption: ['userId', 'unique_name']
});

export const Application = model<IApplicationModel>('Application', ApplicationSchema);

export const CreateApplicationModel = (app: IApplicationModel): IApplicationModel => {
  return new Application({
    ...app,
    _id: new mongoose.mongo.ObjectId(),
    status: 'Starting',
    unique_name: uuid()
  });
};
