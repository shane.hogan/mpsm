import {Document, model, Schema} from 'mongoose';
import * as encrypt from 'mongoose-encryption';
import * as dotenv from 'dotenv';
dotenv.config();
import IProfile from '../interfaces/IProfile';

const encryptSecret =
  'Some Long sentence that I need to store in an envrionment variable so no one can esee it 2345678';
const encKey = process.env.DB_32BYTE_STRING;
const sigKey = process.env.DB_64BYTE_STRING;

export interface IProfileModel extends IProfile, Document {}

const ProfileSchema = new Schema(
  {
    userId: { type: String, required: true },
    aws_region: { type: String, default: 'us-east-1' },
    aws_access_key: { type: String },
    aws_secret: { type: String },
    aws_account_id: { type: String }
  },
  {
    timestamps: {}
  }
);

ProfileSchema.plugin(encrypt, {
  //secret: encryptSecret,
   encryptionKey: encKey,
   signingKey: sigKey,
   excludeFromEncryption: ['userId']
});

export const Profile = model<IProfileModel>('Profile', ProfileSchema);

export const CreateProfile = (
  userId: string,
  aws_access_key: string,
  aws_secret: string,
  aws_account_id: string
): IProfileModel => {
  return new Profile({
    userId,
    aws_region: 'us-east-1',
    aws_access_key,
    aws_secret,
    aws_account_id
  });
};

export const CreateProfileFromCredentialsRow = (
  creds: any,
  userId: string
): IProfileModel => {
  if (!creds.hasOwnProperty('User name'))
    throw new Error('Invalid Credentials file. Missing User Name field.');
  if (!creds.hasOwnProperty('Secret access key'))
    throw new Error("'Invalid Credentials file. Missing Secret access key.");
  if (!creds.hasOwnProperty('Console login link'))
    throw new Error("'Invalid Credentials file. Missing Console login link.");
  const accessKey = creds['Access key ID'];
  const secretAccessKey = creds['Secret access key'];
  const loginLink = creds['Console login link'];
  const matches = /https:\/\/(.*)\.signin.*/gm.exec(loginLink);
  const accountId = matches[1];
  return CreateProfile(userId, accessKey, secretAccessKey, accountId);
};
