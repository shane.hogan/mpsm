import * as express from "express";
import {NextFunction, Request, Response} from "express";
import * as path from "path";
import * as cookieParser from "cookie-parser";
import * as logger from "morgan";
import * as session from "express-session";
import * as ApiRouter from "./routes/api/index";
import * as methodOverride from "method-override";
import * as dotenv from 'dotenv';

class Server {
    public app: express.Application;

    public static boostrap(): Server {
        dotenv.config();
        return new Server();
    }

    constructor() {
        this.app = express();

        this.config();
        this.routes();
        this.app.use(this.errorHanlder.bind(this));
    }

    config() {
        this.app.use(logger("dev"));
        this.app.use(express.json());
        this.app.use(express.urlencoded({ extended: false }));
        this.app.use(cookieParser());
        this.app.use(methodOverride());
        this.app.use(
            session({
                secret: "secret",
                resave: true,
                saveUnitialized: false
            })
        );
    }

    routes() {
        let router: express.Router = express.Router();
        const frontendPath = path.join(__dirname, "../../frontend/build");

        router.use(express.static(frontendPath));

        const api: ApiRouter.Api = new ApiRouter.Api();
        router.use("/api", api.router.bind(api.router));

        //Catch all for react - client side routing
        router.get("*", function(req, res) {
            res.sendFile(`${frontendPath}/index.html`);
        });
        this.app.use(router);
    }

    private errorHanlder(
        err: any,
        req: Request,
        res: Response,
        next: NextFunction
    ) {
        console.error(err.stack);
        res.status(500).send({message: err.message, stack: err.stack});
    }
}

const server = Server.boostrap();
export default server.app;
