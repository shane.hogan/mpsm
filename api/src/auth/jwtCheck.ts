import * as jwt from 'express-jwt';
import * as jwks from 'jwks-rsa';
import {RequestHandler} from 'express';
import * as dotenv from 'dotenv';
dotenv.config();
/**
 * Verifies JWT TToken and injects User obj into `req.user`
 * Sample User below
 */
const jwtCheck: RequestHandler = jwt({
  secret: jwks.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: parseInt(process.env.JWK_RREQUESTS_PER_MIN),
    jwksUri: process.env.JWKS_URI
  }),
  audience: process.env.AUTH_AUDIENCE,
  issuer: process.env.AUTH_ISSUER,
  algorithms: ['RS256']
});

export interface JWTUser {
  iss: string;
  sub: string;
  aud: [string];
  iat: number;
  expr: number;
  azp: string;
  scope: string;
}
/**
{
    "iss": "", Issuer
    "sub": "", Subject (unique user id)
    "aud": [], Audience
    "iat": 1549030810, Issued At
    "exp": 1549038010, Expires At
    "azp": "", Authorized Party
    "scope": "" Scope
}
**/

export default jwtCheck;
