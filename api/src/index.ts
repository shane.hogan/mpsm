import app from "./app";
import * as dotenv from 'dotenv';
import * as mongoose from "mongoose";
dotenv.config();
const port = process.env.PORT || 5000;

app.listen(port, () => {
    const dbUri = process.env.DB_URI;
    console.log(`Attempting to connect to Mongo DB at ${dbUri}`);
    //connect to db
    mongoose.connect(dbUri, { useNewUrlParser: true , useUnifiedTopology: true})
        .then(fulfilled => console.log(`Mongo Connection Established`))
        .catch(rejected => console.log('Mongo Connection Rejected: %o', rejected));

    return console.log(`Listening on Port ${port}`);
});
