import {IApplicationModel} from "../Models/ApplicationModel";

export interface IAWSService {
    testConnection(): Promise<{ error: boolean; errorMessage?: string }>;
    getPipelineStatus(name: string, app: IApplicationModel);
}