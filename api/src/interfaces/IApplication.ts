export default interface IApplication {
  userId: string;
  app_name: string;
  unique_name: string;
  aws_region: string;
  aws_access_key: string;
  aws_secret: string;
  aws_account_id: string;
  git_owner: string;
  git_branch: string;
  git_report: string;
  git_oauth_token: string;
  updatedAt: Date;
  createdAt: Date;
  status: string;
  beanstalkUrl: string;
}
