import {IApplicationModel} from '../Models/ApplicationModel';

export interface IApplicationSevice {
  getOne(id: String, userId: String): Promise<IApplicationModel>;
  getAll(userId: String): Promise<IApplicationModel[]>;
  deleteOne(id: String, userId: String): Promise<IApplicationModel>;
  addOne(app: IApplicationModel): Promise<IApplicationModel>;
  start(app);
  viewLogs(id: String, userId: String): string;
}
