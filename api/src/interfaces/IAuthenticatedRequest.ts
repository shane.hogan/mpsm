import {Request} from 'express';
import {JWTUser} from '../auth/jwtCheck';

export interface IAuthenticatedRequest extends Request {
  user: JWTUser;
}
