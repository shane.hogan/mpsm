export default interface IProfile {
  userId?: string;
  aws_region: string;
  aws_access_key: string;
  aws_secret: string;
  aws_account_id: string;
  updatedAt?: Date;
  createdAt?: Date;
}
