import {IProfileModel} from '../Models/ProfileModel';

export interface IProfileService {
    getOne(id: String): Promise<IProfileModel>;

    updateOne(profile: IProfileModel): Promise<IProfileModel>;

    deleteOne(userId: string): Promise<IProfileModel | null>;

}
