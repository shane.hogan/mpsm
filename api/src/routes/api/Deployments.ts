import {NextFunction, Request, Response, Router} from 'express';
import {Types} from 'mongoose';
import {IApplicationSevice} from '../../interfaces/IApplicationSevice';
import {ApplicationService} from '../../services/ApplicationService';
import {CreateApplicationModel, IApplicationModel} from '../../Models/ApplicationModel';
import {AWSService} from '../../services/AWSService';
import {IAuthenticatedRequest} from '../../interfaces/IAuthenticatedRequest';

namespace Route {
  export class DeploymentsApi {
    router: Router;
    applicationService: IApplicationSevice;

    constructor() {
      this.router = Router();
      this.applicationService = new ApplicationService();

      this.router.get('/all', this.getAllApplication.bind(this));
      this.router.get('/logs/:id', this.getLogs.bind(this));
      this.router.get('/:id', this.getOneApplication.bind(this));
      this.router.delete('/:id', this.stopAndDeleteApplication.bind(this));
      this.router.post('/', this.createApplication.bind(this));
    }

    private stopAndDeleteApplication(req: Request, res: Response) {
      const { id } = req.params;
      // @ts-ignore
      const { sub: userId } = req.user;
      console.log('Trying to delete' + id);
      if (!Types.ObjectId.isValid(id)) {
        return res.status(500).json({ error: 'Invalid Id' });
      }
      this.applicationService
        .deleteOne(id, userId)
        .then(() => {
          res.status(200).json({});
        })
        .catch(err => {
          res.status(500).json(err);
        });
    }

    private getAllApplication(req: IAuthenticatedRequest, res: Response) {
      const { sub: userId } = req.user;
      console.log('Getting applications for ' + userId);
      this.applicationService
        .getAll(userId)
        .then((apps: IApplicationModel[]) => {
          apps.forEach((app: IApplicationModel) => {
            new AWSService(
              app.aws_access_key,
              app.aws_secret,
              app.aws_region,
              app.aws_account_id
            ).getPipelineStatus(app.unique_name, app);
          });
          res.json(apps);
        })
        .catch(err => {
          res.status(500).json(err);
        });
    }

    private getOneApplication(req: IAuthenticatedRequest, res: Response) {
      const { id } = req.params;
      const { sub: userId } = req.user;

      if (!Types.ObjectId.isValid(id)) {
        return res.status(500).json({ error: 'Invalid Id' });
      }
      this.applicationService
        .getOne(id, userId)
        .then(app => {
          res.json(app);
        })
        .catch(err => {
          res.status(500).json(err);
        });
    }

    private async createApplication(
      req: IAuthenticatedRequest,
      res: Response,
      next: NextFunction
    ) {
      const { sub: userId } = req.user;
      const newApp = CreateApplicationModel(req.body as IApplicationModel);
      newApp.userId = userId;
      const errors = (await newApp.validateSync()) as any;
      if (errors) {
        let errMsg = 'Error.\n\n Missing or invalid fields: \n';
        for (let key in errors.errors) {
          errMsg += ' ' + key + '\n';
        }
        return res.status(500).json(errMsg);
      }
      try {
        const valid = await new AWSService(
          newApp.aws_access_key,
          newApp.aws_secret,
          newApp.aws_region,
          newApp.aws_account_id
        ).testConnection();
        if (valid.error) throw Error(valid.errorMessage);
      } catch (e) {
        next(e);
      }
      this.applicationService
        .addOne(newApp)
        .then(async app => {
          res.status(200).json('Creating');
          try {
            const result = await this.applicationService.start(app);
            //res.json(app);
          } catch (err) {
            //res.status(500).json(err.stderr);
            console.log(err);
          }
        })
        .catch(err => {
          res.status(500).json(err);
        });
    }

    private getLogs(req: IAuthenticatedRequest, res: Response) {
      const { id } = req.params;
      const { sub: userId } = req.user;
      if (!Types.ObjectId.isValid(id)) {
        return res.status(500).json({ error: 'Invalid Id' });
      }

      res.json(this.applicationService.viewLogs(id, userId));
    }
  }
}

const Deployments = new Route.DeploymentsApi();
export default Deployments;
