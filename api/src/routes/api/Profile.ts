import {NextFunction, Request, Response, Router} from 'express';
import {IAuthenticatedRequest} from '../../interfaces/IAuthenticatedRequest';
import {IProfileService} from '../../interfaces/IProfileService';
import {ProfileService} from '../../services/ProfileService';
import {CreateProfileFromCredentialsRow, IProfileModel} from '../../Models/ProfileModel';
import {AWSService} from '../../services/AWSService';
import * as multer from 'multer';
import * as path from 'path';
import * as parse from 'csv-parse';

export class ProfileApi {
  router: Router;
  profileService: IProfileService;
  multerUpload: multer.Instance;

  constructor() {
    this.router = Router();

    const memoryStorage = multer.memoryStorage();
    this.multerUpload = multer({
      storage: memoryStorage,
      fileFilter(
        req: Express.Request,
        file: Express.Multer.File,
        callback: (error: Error | null, acceptFile: boolean) => void
      ): void {
        const ext = path.extname(file.originalname);
        const csv = '.csv';
        if (ext !== csv) {
          const error = new Error('Only CSV Files are allowed');
          return callback(error, false);
        }
        return callback(null, true);
      }
    });

    this.profileService = new ProfileService();
    this.router.post(
      '/credentialsFile',
      this.multerUpload.single('credentials'),
      this.uploadCredentialsFile.bind(this)
    );
    this.router.post('/', this.updateUserProfile.bind(this));
    this.router.get('/', this.getUserProfile.bind(this));
    this.router.delete('/', this.deleteProfile.bind(this));
  }

  private deleteProfile(req: Request, res: Response, next: NextFunction){
      // @ts-ignore
      const { sub: userId } = req.user;
      try {
          const deleted = this.profileService.deleteOne(userId);
          res.status(200).json(deleted);
      }
      catch(e){
        next(e);
      }
  }

  private uploadCredentialsFile(req: Request, res: Response, next: NextFunction) {
    const file = req.file;
    if(!file){
      throw new Error('No File was selected. Please select a valid CSV file.');
    }
    // @ts-ignore
    const { sub: userId } = req.user;
    parse(
      file.buffer.toString('utf-8'),
      {
        delimiter: ',',
        columns: true
      },
      async (err, records) => {
        try {
          if (err) throw new Error(err.message);
          if (records.length > 1)
            throw new Error('Please upload a credentials file with only ONE record.');
          const profile = CreateProfileFromCredentialsRow(records[0], userId);
          const valid = await new AWSService(
            profile.aws_access_key,
            profile.aws_secret,
            profile.aws_region,
            profile.aws_account_id
          ).testConnection();
          if (valid.error) throw new Error(valid.errorMessage);
          else {
            const updatedProfile = await this.profileService.updateOne(profile);
            res.status(200).json(updatedProfile);
          }
        } catch (e) {
          next(e);
        }
      }
    );
  }

  private async getUserProfile(req: Request, res: Response) {
    // @ts-ignore
    const { sub: userId } = req.user;
    const profile = await this.profileService.getOne(userId);
    res.status(200).json(profile);
  }

  private async updateUserProfile(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    const { sub: userId } = req.user;
    let old_profile = req.body as IProfileModel;
    old_profile.userId = userId;
    try {
      const valid = await new AWSService(
        old_profile.aws_access_key,
        old_profile.aws_secret,
        old_profile.aws_region,
        old_profile.aws_account_id
      ).testConnection();
      if (valid.error) throw Error(valid.errorMessage);
      const profile = await this.profileService.updateOne(old_profile);
      res.status(200).json(profile);
    } catch (e) {
      next(e);
    }
  }
}

const Profile = new ProfileApi();
export default Profile;
