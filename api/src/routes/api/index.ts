import {Request, Response, Router} from 'express';
import Deployments from './Deployments';
import Profile from './Profile';
import jwtCheck from "../../auth/jwtCheck";

namespace ApiRoute {
  export class Api {
    public router: Router;

    constructor() {
      this.router = Router();
      this.router.get('/version', (req: Request, res: Response) => {
        res.json({
          version: '1.0.0'
        });
      });
      this.router.use(jwtCheck);
      this.router.use('/deployments', Deployments.router.bind(Deployments.router));
      this.router.use('/profile', Profile.router.bind(Profile.router));
    }
  }
}

export = ApiRoute;
