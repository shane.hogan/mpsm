import * as AWS from 'aws-sdk';
import {IApplicationModel} from '../Models/ApplicationModel';
import {IAWSService} from "../interfaces/IAWSService";

export class AWSService implements IAWSService{
  private pipeline: AWS.CodePipeline;
  private accountId: string;
  constructor(accessKey: string, secret: string, region: string, accountId: string) {
    AWS.config.update({
      accessKeyId: accessKey,
      secretAccessKey: secret,
      region: region
    });
    this.accountId = accountId;
    this.pipeline = new AWS.CodePipeline();
  }

  async testConnection(): Promise<{ error: boolean; errorMessage?: string }> {
    try {
      const result = await this.pipeline.getPipeline({ name: '@' }).promise();
      return { error: false };
    } catch (e) {
      console.log(e.message);
      const matches = /Account '(.*)' does not have a pipeline with name '@'/gm.exec(
        e.message
      );
      if (matches && matches[1] && this.accountId) {
        if (this.accountId === matches[1]) return { error: false };
      }
      return { error: true, errorMessage: e.message };
    }
  }

  getPipelineStatus(name: string, app: IApplicationModel) {
    this.pipeline.getPipelineState(
      { name: `${name}_pipeline` },
      (err: AWS.AWSError, data: AWS.CodePipeline.Types.GetPipelineStateOutput) => {
        if (data && data.stageStates && data.stageStates.length === 3) {
          console.log(JSON.stringify(data));
          let sourceStage = data.stageStates[0];
          let buildStage = data.stageStates[1];
          let deployStage = data.stageStates[2];

          if (
            sourceStage &&
            sourceStage.latestExecution &&
            sourceStage.latestExecution.status
          ) {
            if (sourceStage.latestExecution.status === 'InProgress')
              app.status = 'Pulling';
            if (sourceStage.latestExecution.status === 'Failed') app.status = 'Errored';
          }

          if (
            buildStage &&
            buildStage.latestExecution &&
            buildStage.latestExecution.status
          ) {
            if (buildStage.latestExecution.status === 'InProgress')
              app.status = 'Building & Testing';
            if (buildStage.latestExecution.status === 'Failed') app.status = 'Errored';
          }

          if (
            deployStage &&
            deployStage.latestExecution &&
            deployStage.latestExecution.status
          ) {
            if (deployStage.latestExecution.status === 'Succeeded')
              app.status = 'Running';
            if (deployStage.latestExecution.status === 'InProgress')
              app.status = 'Deploying';
            if (deployStage.latestExecution.status === 'Failed') app.status = 'Errored';
          }
          app.save();
        }
      }
    );
  }
}
