import {expect} from 'chai';
import 'mocha';
import * as mongoose from 'mongoose';
import {connection} from 'mongoose';
import {ApplicationService} from './ApplicationService';
import {IApplicationSevice} from '../interfaces/IApplicationSevice';
import {Application, IApplicationModel} from '../Models/ApplicationModel';

let service: IApplicationSevice;
let collection: mongoose.Connection;

beforeEach('Connect to DB', () => {
  service = new ApplicationService();
  mongoose
    .connect('mongodb://127.0.0.1:27017/MPSM-test', { useNewUrlParser: true })
    .then(fulfilled => {
      console.log(`Mongo Connection Established`);
      collection = mongoose.connection;
    })
    .catch(rejected => console.log('Mongo Connection Rejected: %o', rejected));
});

describe('API Starts', () => {
  it('should start', async () => {
    const apps = await service.getAll('sdfgh');
    expect(apps).to.have.length(0);
  });
  it('should insert one', async () => {
    let newApp: IApplicationModel = new Application({
      app_name: 'Test',
      unique_name: 'blank',
      aws_region: 'dfgh',
      aws_account_id: '12345678',
      aws_secret: 'dfghjk',
      aws_access_key: 'sdfghjk',
      git_owner: 'sdfghj',
      git_oauth_token: 'qwertyuiopasdfghjkl',
      git_branch: 'qwertyuiopsdfghjk'
    });
    const saved = await service.addOne(newApp);
    const allApps = await service.getAll('sdfgh');
    expect(allApps).to.have.length(1);
  });
});

afterEach('Empty DB', async () => {
  await connection.db.dropDatabase();
});
