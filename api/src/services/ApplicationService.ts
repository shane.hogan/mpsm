import {IApplicationSevice} from '../interfaces/IApplicationSevice';
import {Application, IApplicationModel} from '../Models/ApplicationModel';
import * as path from 'path';
import * as fs from 'fs';
import * as fse from 'fs-extra';

import {execSync} from 'child_process';

const util = require('util');
const exec = util.promisify(require('child_process').exec);
const options = { encoding: 'utf8' };
export class ApplicationService implements IApplicationSevice {
  getAll(userId: string): Promise<IApplicationModel[]> {
    return Application.find({ userId: userId }).exec();
  }

  getOne(id: String, userId: string): Promise<IApplicationModel> {
    return Application.findOne({ _id: id, userId: userId }).exec();
  }

  async deleteOne(id: String, userId: string): Promise<IApplicationModel> {
    const app = await Application.findOne({ _id: id, userId: userId });
    if (!app) throw Error('No App Found');
    const workDir = path.join(__dirname, `../../terra-deployments/${id}`);
    try {
      execSync(
        `cd ${workDir} && docker run --privileged -v $(pwd):/app/ -w /app/ hashicorp/terraform:full destroy -auto-approve -force`
      );
      //execSync(`rm -rf ${workDir}`);
      return Application.findOneAndDelete({ _id: id }).exec();
    } catch (e) {
      if (!fs.existsSync(`${workDir}/terraform.tfstate`)) {
        console.log('Terraform was never initialized, safe to delete without stopping');
        //execSync(`rm -rf ${workDir}`);
        return Application.findOneAndDelete({ _id: id }).exec();
      }
      throw e;
    }
  }

  async addOne(app: IApplicationModel): Promise<IApplicationModel> {
    return Application.create(app);
  }

  viewLogs(id: String, userId: string): string {
    const app = Application.findOne({ _id: id, userId: userId })
      .then(app => {
        if (!app) {
          throw Error('No App Found');
        }
      })
      .catch(e => {
        throw Error('No App Found');
      });

    return fs.readFileSync(
      path.join(__dirname, `../../terra-deployments/${id}/logfile.txt`),
      { encoding: 'utf8' }
    );
  }

  async start(app: IApplicationModel) {
    const workDir = path.join(__dirname, `../../terra-deployments/${app._id}`);
    const logFile = `${workDir}/logfile.txt`;
    fse.mkdirSync(workDir);
    fse.writeFileSync(logFile, 'Copying over terraform files....');
    fse.copySync(`${path.join(__dirname, '../../terraform/')}`, workDir);
    fse.removeSync(`${workDir}/terraform.tfvars`);
    fse.removeSync(`${workDir}/terraform.tfstate`);
    fse.removeSync(`${workDir}/terraform.tfstate.backup`);
    fse.removeSync(`${workDir}/.terraform/`);

    fse.writeFileSync(
      `${workDir}/terraform.tfvars`,
      `app_name="${app.app_name}"
unique_name="${app.unique_name}"
aws_region="${app.aws_region}"
aws_account_id="${app.aws_account_id}"
aws_access_key="${app.aws_access_key}"
aws_secret="${app.aws_secret}"
git_branch="${app.git_branch}"
git_oauth_token="${app.git_oauth_token}"
git_owner="${app.git_owner}"
git_repo="${app.git_report}"`
    );
    try {
      const init = await exec(
        `docker run --privileged -v $(pwd):/app/ -w /app/ hashicorp/terraform:full init`,
        { cwd: workDir }
      );
      fse.appendFileSync(logFile, init.stdout);
      fse.appendFileSync(logFile, init.stderr);
      // const {stdout, stderr} = await exec(`docker run --privileged -v $(pwd):/app/ -w /app/ hashicorp/terraform:full plan`, {cwd: workDir});
      // fse.appendFileSync(logFile, stdout);
      // fse.appendFileSync(logFile, stderr);
      const applyOut = await exec(
        `docker run --privileged -v $(pwd):/app/ -w /app/ hashicorp/terraform:full apply -auto-approve`,
        { cwd: workDir }
      );
      fse.appendFileSync(logFile, applyOut.stdout);
      fse.appendFileSync(logFile, applyOut.stderr);
      app.beanstalkUrl = this.getBeanstalkUrl(applyOut.stdout);
      if (app.beanstalkUrl === '') {
        console.log('Use stderr instead');
        app.beanstalkUrl = this.getBeanstalkUrl(applyOut.stderr);
      }
      app.status = 'Running';
      app.save();
    } catch (err) {
      fse.appendFileSync(logFile, 'Error occurred while initializing terraform');
      fse.appendFileSync(logFile, err.stderr);
      app.status = 'Errored';
      app.save();
      //await this.deleteOne(app._id);
      throw err;
    }
  }

  getBeanstalkUrl = (output: string): string => {
    const regex = /Beanstalk Env URL = (.*\.elasticbeanstalk.com)/g;
    let matches = regex.exec(output);
    if (matches[1]) return matches[1];
    return '';
  };

  normalizeAppName = (appName: string): string => {
    let normal = '';
    normal = appName.replace(' ', '');
    return normal;
  };
}
