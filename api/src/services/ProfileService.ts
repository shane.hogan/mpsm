import {IProfileService} from '../interfaces/IProfileService';
import {IProfileModel, Profile} from '../Models/ProfileModel';

export class ProfileService implements IProfileService {
  getOne(id: String): Promise<IProfileModel> {
    return Profile.findOne({ userId: id }).exec();
  }

  createOne(profile: IProfileModel): Promise<IProfileModel> {
    console.log(`Creating new Profile`);
    return Profile.create(profile);
  }

  deleteOne(userId: string): Promise<IProfileModel | null> {
    return Profile.findOneAndDelete({ userId: userId }).exec();
  }
  /**
   * We can't update encrypted fields so we have to delete and recreate
   * @param profile
   */
  async updateOne(profile: IProfileModel): Promise<IProfileModel> {
    console.log(profile);
    await this.deleteOne(profile.userId);
    return this.createOne(profile);
  }
}
