resource "aws_elastic_beanstalk_application" "beanstalk-application" {
  name = "${lower(local.unique_app_name)}"
  //  "appversion_lifecycle" {
  //    service_role = "${aws_iam_role.terraformrole.arn}"
  //  }
  lifecycle {
    ignore_changes = ["name"]
  }
}

resource "aws_elastic_beanstalk_configuration_template" "beanstalk-template" {
  name = "${lower(local.unique_app_name)}"
  application = "${aws_elastic_beanstalk_application.beanstalk-application.name}"
  solution_stack_name = "64bit Amazon Linux 2018.03 v2.12.7 running Docker 18.06.1-ce"
  lifecycle {
    ignore_changes = ["name"]
  }
}

resource "aws_elastic_beanstalk_environment" "dev-env" {
  name = "${lower(local.unique_app_name)}"
  application = "${aws_elastic_beanstalk_application.beanstalk-application.name}"
  template_name = "${aws_elastic_beanstalk_configuration_template.beanstalk-template.name}"
  lifecycle {
    ignore_changes = ["name", "application", "template_name"]
  }
  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name = "IamInstanceProfile"
    value = "${aws_iam_instance_profile.beanstalk_ec2.name}"
  }
}
