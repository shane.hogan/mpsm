provider "aws" {
  region = "${var.aws_region}"
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret}"
}

output "AppName" {
  value = "AppName is ${var.app_name}"
}

output "Beanstalk Env URL" {
  value = "${aws_elastic_beanstalk_environment.dev-env.cname}"
}