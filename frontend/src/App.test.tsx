import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Auth from './Auth/Auth';
import {Provider} from 'react-redux';
import store from './redux/store';

const auth = new Auth();

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(
    <Provider store={store}>
      <App auth={auth} />
    </Provider>,
    div
  );
  ReactDOM.unmountComponentAtNode(div);
});
