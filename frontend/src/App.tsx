import React, {Component} from 'react';
import {BrowserRouter as Router} from 'react-router-dom';
import {Grid} from '@material-ui/core';
import AppBar from './components/AppBar';
import AppFooter from './components/AppFooter';
import './App.css';
import Auth from './Auth/Auth';
import {connect} from 'react-redux';
import Routes from './Routes';
import {ResizeSensor} from "@blueprintjs/core";

interface AppProps {
  auth: Auth;
}

interface StateProps {
  auth0: any;
}

interface DispatchProps {}

type ComponentProps = AppProps & StateProps & DispatchProps;
interface OwnState {
    smallScreen: boolean
}

class App extends Component<ComponentProps, OwnState> {

  constructor(props: ComponentProps){
    super(props);
    this.state = {
      smallScreen: false
    };
  }

  componentDidMount(): void {
    this.props.auth.checkSession();
  }

    handleResize = (entries: any) => {
        const { smallScreen } = this.state;
        const e = entries[0];
        if (e.contentRect.width <= 768 && !smallScreen) {
            this.setState({ smallScreen: true });
        } else if (smallScreen) {
            this.setState({ smallScreen: false });
        }
    };

  render() {
    const { auth, auth0 } = this.props;
    const {smallScreen} = this.state;
    return (
        <ResizeSensor onResize={this.handleResize}>
        <Grid
        container
        direction={'column'}
        justify={'space-between'}
        alignItems={'stretch'}
        style={{ height: '100%', flexWrap: 'nowrap' }}
      >
        <Grid item>
          <Router>
            <Grid container justify={'center'}>
              <Grid item style={{ width: '100%' }}>
                <AppBar auth={auth} authenticated={!!auth0} smallScreen={smallScreen}/>
              </Grid>
              <Grid
                item
                style={{ height: '100%', width: '100%' }}
                className={'ca-container'}
              >
                <Routes auth0={auth0} auth={auth} />
              </Grid>
            </Grid>
          </Router>
        </Grid>
        <Grid item>
          <AppFooter />
        </Grid>
      </Grid>
        </ResizeSensor>
    );
  }
}

const mapStateToProps = (state: any): StateProps => ({
  auth0: state.auth0
});
const mapDispatchToProps = (dispatch: any): DispatchProps => ({});

export default connect<StateProps, DispatchProps, AppProps>(
  mapStateToProps,
  mapDispatchToProps
)(App);
