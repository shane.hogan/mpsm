import lock from './Auth0Lock.config';
import { Auth0Error } from 'auth0-js';
import store from '../redux/store';
import { clearAuth0Profile, setAuth0Profile } from '../redux/auth0.reducer';
import moment from 'moment';

export const ACCESS_TOKEN = 'MPSM_ACCESS_TOKEN';
class Auth {
  lock: Auth0LockStatic = lock;
  accessToken: string | null = null;
  idToken: string | null = null;
  expiresAt: moment.Moment | null = null;

  constructor() {
    this.logout = this.logout.bind(this);
    this.checkSession = this.checkSession.bind(this);
    this.renewSession = this.renewSession.bind(this);
    this.isAuthenticated = this.isAuthenticated.bind(this);
    this.listen();
  }

  listen() {
    this.lock.on('authenticated', (authResult: AuthResult) => {
      this.setValues(authResult);
    });
  }

  renewSession() {
    this.checkSession();
  }

  logout() {
    this.lock.logout({ returnTo: window.location.origin });
    this.clearValues();
  }

  checkSession() {
    this.lock.checkSession(
      {},
      (error: Auth0Error, authResult: AuthResult | undefined) => {
        if (error || authResult === undefined) {
          this.clearValues();
        } else {
          this.setValues(authResult);
        }
      }
    );
  }

  isAuthenticated(): boolean {
    let authenticated = false;
    const expiresAt = this.expiresAt;
    if (expiresAt) {
      authenticated = moment().isBefore(expiresAt);
    } else {
      this.checkSession();
    }
    return authenticated;
  }

  private setValues(authResult: AuthResult) {
    this.accessToken = authResult.accessToken;
    this.idToken = authResult.idToken;
    this.expiresAt = moment().add(authResult.expiresIn, 'seconds');
    localStorage.setItem(ACCESS_TOKEN, authResult.accessToken);
    store.dispatch(setAuth0Profile(authResult.idTokenPayload));
  }

  private clearValues() {
    this.accessToken = null;
    this.idToken = null;
    this.expiresAt = null;
    store.dispatch(clearAuth0Profile);
    localStorage.removeItem(ACCESS_TOKEN);
  }
}
export default Auth;
