import Auth0Lock from 'auth0-lock';
import logo from '../assets/cs-logo.svg';

const clientId = process.env.REACT_APP_AUTH_CLIENT_ID || '';
const domain = process.env.REACT_APP_AUTH_DOMAIN || '';
const redirectUri = process.env.REACT_APP_AUTH_REDIRECT_URI || '';
const audience = process.env.REACT_APP_AUTH_AUDIENCE || '';

if (process.env.NODE_ENV !== 'production') {
  console.log(
    `Creating Auth0Lock with config: clientId: ${clientId}, domain: ${domain}, redirectUri: ${redirectUri}, audience: ${audience}`
  );
}

export default new Auth0Lock(clientId, domain, {
  allowSignUp: true,
  language: 'en',
  allowedConnections: [],
  autofocus: true,
  auth: {
    redirectUrl: redirectUri,
    responseType: 'id_token token',
    params: {
      scope:
        'openid email profile user_metadata app_metadata picture read:applications create:applications'
    },
    audience: audience
  },
  allowShowPassword: true,
  theme: {
    logo: logo,
    primaryColor: '#3A99DB'
  }
});
