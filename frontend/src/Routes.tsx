import React from 'react';
import AllApplications from './pages/AllApplications/AllApplications.container';
import ViewLogFile from './pages/ViewLogFile';
import Profile from './pages/Profile/Profile.container';
import {Route, Switch} from 'react-router';
import About from './pages/About';
import CreateApplication from './pages/CreateApplication/CreateApplication.container';
import NotAuthenticated from './pages/NotAuthenticated';
import Callback from './pages/Callback';
import AuthRoute from './components/AuthRoute';
import {useAuth0} from "./auth0-spa";

interface Props {
  auth0: any;
  auth: any;
}

const Routes: React.FunctionComponent<Props> = ({ auth, auth0 }) => {
    const { isAuthenticated, loginWithRedirect } = useAuth0();

    return (
    <Switch>
      <AuthRoute path={'/all'} component={AllApplications} authenticated={isAuthenticated} />
      <AuthRoute
        path={'/create'}
        authenticated={isAuthenticated}
        render={props => <CreateApplication profile={undefined} {...props} />}
      />
      <AuthRoute path={'/logs/:id'} component={ViewLogFile} authenticated={isAuthenticated} />
      <AuthRoute path={'/profile'} component={Profile} authenticated={isAuthenticated} />
      <Route
        path={'/NotAuthenticated/:desiredPath'}
        render={props => <NotAuthenticated auth={auth} loginWithRedirect={loginWithRedirect} />}
      />
      <Route
        path={'/callback'}
        render={() => {
          return <Callback/>;
        }}
      />
      <Route path={'/'} component={About} />
    </Switch>
  );
};

export default Routes;
