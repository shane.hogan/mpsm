import { create } from 'apisauce';
import { ACCESS_TOKEN } from '../Auth/Auth';

export const api = () => {
  const jwtToken = localStorage.getItem(ACCESS_TOKEN);
  return create({
    baseURL: '/api',
    headers: {
      Authorization: `Bearer ${jwtToken}`
    }
  });
};
