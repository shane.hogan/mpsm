import { api } from './ActionUtils';

export const GetCreatedApps = () => {
  return api().get('/deployments/all');
};

export const CreateNewApp = (app: any) => {
  return api().post('/deployments/', app);
};

export const StopAndDeleteApp = (id: string) => {
  return api().delete(`/deployments/${id}`);
};

export const GetLogs = (id: string) => {
  return api().get(`/deployments/logs/${id}`);
};
