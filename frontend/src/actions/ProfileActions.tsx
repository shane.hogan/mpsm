import { api } from './ActionUtils';
import IProfile from '../../../api/src/interfaces/IProfile';
import { ApiResponse } from 'apisauce';

export const GetProfile = (): Promise<ApiResponse<IProfile>> => {
  return api().get(`/profile`);
};

export const UpdateProfile = (profile: IProfile) => {
  return api().post(`/profile`, profile);
};

export const UploadCredentialsFile = (file: File) => {
  let data = new FormData();
  data.append('credentials', file);
  return api().post('/profile/credentialsFile', data);
};

export const DeleteProfile = () => {
  return api().delete('/profile');
};
