import React from 'react';
import {
    Alignment,
    Button,
    Classes, Menu, MenuItem,
    Navbar,
    NavbarDivider,
    NavbarGroup,
    NavbarHeading, Popover
} from '@blueprintjs/core';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import styled from 'styled-components';
import Auth from '../Auth/Auth';
import { useAuth0 } from "../auth0-spa";

const Heading = styled(NavbarHeading)`
  font-weight: bolder;
`;

interface AppBarProps extends RouteComponentProps {
  auth: Auth;
  authenticated: boolean;
    smallScreen: boolean;
}
const renderMenuItems = (authenticated: boolean, history: any, loginWithRedirect: () => {}, logout: () => {}) => {
  return (
      <>
    <Button
    className={Classes.MINIMAL}
    icon='home'
    text='Home'
    onClick={() => history.push('/')}
    />
{authenticated && (
    <>
    <Button
    className={Classes.MINIMAL}
    icon='add'
    text='New App'
    onClick={() => history.push('/create')}
    />
    <Button
    className={Classes.MINIMAL}
    icon='list-detail-view'
    text={'View Apps'}
    onClick={() => history.push('/all')}
    />
    </>
    )}
{!authenticated && (
    <Button
        className={Classes.MINIMAL}
        icon={'log-in'}
        text={'Login'}
        onClick={() => loginWithRedirect()}
    />
)}
{authenticated && (
    <Button
        className={Classes.MINIMAL}
        icon={'user'}
        text={'Profile'}
        onClick={() => history.push('/profile')}
    />
)}
{authenticated && (
    <Button
        className={Classes.MINIMAL}
        icon={'log-out'}
        text={'Logout'}
        onClick={() => logout()}
    />
)}
</>
  )
};

const renderSmallMenuItems = (authenticated: boolean, history: any, loginWithRedirect: () => {}, logout: () => {}) => {
    return (
        <>
            <MenuItem
                className={Classes.MINIMAL}
                icon='home'
                text='Home'
                onClick={() => history.push('/')}
            />
            {authenticated && (
                <>
                    <MenuItem
                        className={Classes.MINIMAL}
                        icon='add'
                        text='New App'
                        onClick={() => history.push('/create')}
                    />
                    <MenuItem
                        className={Classes.MINIMAL}
                        icon='list-detail-view'
                        text={'View Apps'}
                        onClick={() => history.push('/all')}
                    />
                </>
            )}
            {!authenticated && (
                <MenuItem
                    className={Classes.MINIMAL}
                    icon={'log-in'}
                    text={'Login'}
                    onClick={() => loginWithRedirect()}
                />
            )}
            {authenticated && (
                <MenuItem
                    className={Classes.MINIMAL}
                    icon={'user'}
                    text={'Profile'}
                    onClick={() => history.push('/profile')}
                />
            )}
            {authenticated && (
                <MenuItem
                    className={Classes.MINIMAL}
                    icon={'log-out'}
                    text={'Logout'}
                    onClick={() => logout()}
                />
            )}
        </>
    )
};


const AppBar: React.FunctionComponent<AppBarProps> = ({
  history,
  authenticated,
  auth,
  smallScreen
}) => {
    const { isAuthenticated, loginWithRedirect, logout } = useAuth0();

    return (
    <Navbar className={'bp3-dark'}>
      <NavbarGroup align={Alignment.LEFT}>
        <Heading>DevOps Demo</Heading>
        <NavbarDivider />
      </NavbarGroup>
      <NavbarGroup align={Alignment.RIGHT}>
          { !smallScreen &&  renderMenuItems(isAuthenticated, history, loginWithRedirect, logout)}
          { smallScreen && (
                  <Popover>
                      <Button className="bp3-minimal" icon="layout-grid" />
                      <Menu>
                          {renderSmallMenuItems(isAuthenticated, history, loginWithRedirect, logout)}
                      </Menu>
                  </Popover>
          )}
      </NavbarGroup>
    </Navbar>
  );
};

export default withRouter(AppBar);
