import React from 'react';
import { H4 } from '@blueprintjs/core';
import { Grid } from '@material-ui/core';
import ReactSVG from 'react-svg';
import styled from 'styled-components';

import logo from '../assets/cs-logo.svg';

const Footer = styled.footer`
  bottom: 0;
  color: #fff;
  //position: absolute;
  right: 0;
  left: 0;
  padding: 1rem;
  text-align: center;
  background: #2196f3 linear-gradient(40deg, #037da5, #303f9f) !important;
  h4 {
    color: white;
  }
`;

const AppFooter = () => {
  return (
    <Footer>
      <>
        <Grid container justify={'center'} alignItems={'center'}>
          <Grid item sm={12} md={6}>
            <H4>Disclaimer</H4>
            <p>
              This demo is a proof of concept that we (clearAvenue, LLC) have built to
              demonstrate our Application. We have not included production-level security,
              nor any production-level data. If you would like to see more, we are more
              than happy to demonstrate the features in a formal walkthrough. To talk to a
              member of our team about this, please contact us at
              http://www.clearavenue.com/contact.html
            </p>
          </Grid>
          <Grid item sm={12} md={6}>
            <ReactSVG src={logo}/>
          </Grid>
          <Grid item sm={12}>
            <H4>Copyright © 2018, clearAvenue LLC.</H4>
          </Grid>
        </Grid>
      </>
    </Footer>
  );
};

export default AppFooter;
