import React from 'react';
import { Button, Card, Classes, Elevation, H5, Intent, Tag } from '@blueprintjs/core';
import { Grid } from '@material-ui/core';
import { IApplicationModel } from '../../../api/src/Models/ApplicationModel';
import moment from 'moment';

interface CardProps {
  stopAndDelete: (id: string) => void;
  logs: (id: string) => void;
  app: IApplicationModel;
}

const GetIntent = (status: string): 'success' | 'danger' | 'none' => {
  switch (status) {
    case 'Running':
      return Intent.SUCCESS;
    case 'Errored':
      return Intent.DANGER;
    default:
      return Intent.NONE;
  }
};

const ApplicationCard: React.FunctionComponent<CardProps> = ({
  app,
  stopAndDelete,
  logs
}) => {
  return (
    <Card elevation={Elevation.TWO}>
      <Grid container justify={'space-between'}>
        <Grid item>
          <H5>{app.app_name}</H5>
        </Grid>
        <Grid item>
          <Tag intent={GetIntent(app.status)} minimal={true} round={true}>
            {app.status}
          </Tag>
        </Grid>
      </Grid>
      <p className={'bp3-monospace-text'}>
        AWS Account Id: {app.aws_account_id}
        <br />
        AWS Region: {app.aws_region}
        <br />
        App Name: {app.app_name}
        <br />
        Created: {moment(app.createdAt).format('MMM DD, YYYY')}
        <br />
        Last Modified: {moment(app.updatedAt).format('MMM DD, YYYY')}
        <br />
      </p>

      {app.beanstalkUrl && app.status === 'Running' && (
        <a href={`http://${app.beanstalkUrl}`} target={'_blank'}>
          Visit {app.app_name}
        </a>
      )}

      <Grid container justify={'space-between'} alignItems={'center'}>
        <Grid item>
          <Button intent={Intent.DANGER} onClick={() => stopAndDelete(app._id)}>
            Stop & Delete
          </Button>
        </Grid>
        <Grid item>
          <Button onClick={() => logs(app._id)}>View Logs</Button>
        </Grid>
      </Grid>
    </Card>
  );
};

export const LoadingCard = () => (
  <Card elevation={Elevation.TWO}>
    <H5 className={Classes.SKELETON}>Waiting</H5>
    <p className={Classes.SKELETON}>
      <br />
      <br />
      <br />
      <br />
    </p>

    <Grid container justify={'space-between'} alignItems={'center'}>
      <Grid item className={Classes.SKELETON}>
        <Button>Waiting</Button>
      </Grid>

      <Grid item className={Classes.SKELETON}>
        <Button>Waiting</Button>
      </Grid>
    </Grid>
  </Card>
);
export default ApplicationCard;
