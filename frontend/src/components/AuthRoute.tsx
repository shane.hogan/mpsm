import React from 'react';
import { Redirect, Route, RouteProps } from 'react-router';

interface AuthRouteProps extends RouteProps {
  authenticated: boolean;
}

const AuthRoute: React.FunctionComponent<AuthRouteProps> = ({
  authenticated,
  ...rest
}) => (
  <>
    {authenticated && <Route {...rest} />}
    {!authenticated && <Redirect to={`/NotAuthenticated${rest.path}`} />}
  </>
);

export default AuthRoute;
