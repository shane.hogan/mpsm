import React from 'react';
import { Field, InjectedFormProps, reduxForm, WrappedFieldsProps } from 'redux-form';
import { renderEditableText, renderTextInput, Validator } from './FormControls';
import { Button, Divider, H1, Intent } from '@blueprintjs/core';

const CreateApplicationForm: React.FunctionComponent<InjectedFormProps> = ({
  handleSubmit
}) => {
  return (
    <form onSubmit={handleSubmit}>
      <H1>
        <Field
          name={'app_name'}
          component={
            renderEditableText as 'input' & React.FunctionComponent<WrappedFieldsProps>
          }
          placeholder={'App Name...'}
        />
      </H1>
      <Divider />
      <Field
        label={'Application Name'}
        component={
          renderTextInput as 'input' & React.FunctionComponent<WrappedFieldsProps>
        }
        required={true}
        name={'app_name'}
        validate={[Validator.required]}
        labelinfo={'(required)'}
      />
    <Field
        label={'GitHub Repository'}
        component={
            renderTextInput as 'input' & React.FunctionComponent<WrappedFieldsProps>
        }
        required={true}
        name={"git_report"}
        validate={[Validator.required]}
        labelinfo={'(required)'}
        />
        <Field
            label={'Repository Branch'}
            component={
                renderTextInput as 'input' & React.FunctionComponent<WrappedFieldsProps>
            }
            required={true}
            name={"git_branch"}
            validate={[Validator.required]}
            labelinfo={'(required)'}
        />
      <Field
        label={'AWS Account Id'}
        required={true}
        component={
          renderTextInput as 'input' & React.FunctionComponent<WrappedFieldsProps>
        }
        name={'aws_account_id'}
        validate={[Validator.required]}
        labelinfo={'(required)'}
      />

      <Field
        label={'AWS Access Key'}
        required={true}
        component={
          renderTextInput as 'input' & React.FunctionComponent<WrappedFieldsProps>
        }
        name={'aws_access_key'}
        validate={[Validator.required]}
        labelinfo={'(required)'}
      />

      <Field
        label={'AWS Secret'}
        required={true}
        component={
          renderTextInput as 'input' & React.FunctionComponent<WrappedFieldsProps>
        }
        name={'aws_secret'}
        validate={[Validator.required]}
        labelinfo={'(required)'}
      />

      <Button intent={Intent.PRIMARY} type={'submit'}>
        Submit
      </Button>
    </form>
  );
};

export default reduxForm({
  form: 'CreateApplicationForm'
})(CreateApplicationForm);
