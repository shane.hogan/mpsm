import React from 'react';
import { Button, Divider, H2, Intent } from '@blueprintjs/core';
import { Field, InjectedFormProps, reduxForm, WrappedFieldsProps } from 'redux-form';
import { renderTextInput, Validator } from './FormControls';

const DefaultSettingsForm: React.FunctionComponent<InjectedFormProps> = ({
  handleSubmit
}) => {
  return (
    <>
      <H2>Default AWS Settings</H2>
      <Divider />
      <form onSubmit={handleSubmit}>
        <Field
          label={'AWS Account Id'}
          required={true}
          component={
            renderTextInput as 'input' & React.FunctionComponent<WrappedFieldsProps>
          }
          name={'aws_account_id'}
          validate={[Validator.required]}
          labelinfo={'(required)'}
        />
        <Field
          label={'AWS Access Key'}
          required={true}
          component={
            renderTextInput as 'input' & React.FunctionComponent<WrappedFieldsProps>
          }
          name={'aws_access_key'}
          validate={[Validator.required]}
          labelinfo={'(required)'}
        />
        <Field
          label={'AWS Secret'}
          required={true}
          component={
            renderTextInput as 'input' & React.FunctionComponent<WrappedFieldsProps>
          }
          name={'aws_secret'}
          validate={[Validator.required]}
          labelinfo={'(required)'}
        />
        <Button intent={Intent.PRIMARY} type={'submit'}>
          Submit
        </Button>
      </form>
    </>
  );
};
export default reduxForm({
  form: 'defaultSettings'
})(DefaultSettingsForm);
