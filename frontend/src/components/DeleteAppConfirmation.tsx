import React, { FunctionComponent } from 'react';
import { Alert, Intent } from '@blueprintjs/core';
import { IApplicationModel } from '../../../api/src/Models/ApplicationModel';

interface AlertProps {
  isOpen: boolean;
  app: IApplicationModel;
  doCancel: () => void;
  doConfirm: (id: string) => void;
}

const DeleteAppConfirmation: FunctionComponent<AlertProps> = ({
  isOpen,
  app,
  doConfirm,
  doCancel
}) => {
  return (
    <Alert
      canEscapeKeyCancel={true}
      canOutsideClickCancel={true}
      cancelButtonText='Cancel'
      confirmButtonText='Yes, Stop & Delete'
      icon='trash'
      intent={Intent.DANGER}
      isOpen={isOpen}
      onCancel={() => doCancel()}
      onConfirm={() => doConfirm(app._id)}
    >
      <p>
        Are you sure you want to Stop & Delete{' '}
        <b>
          <i>{app.app_name}</i>
        </b>
        ? This action cannot be undone.
      </p>
    </Alert>
  );
};

export default DeleteAppConfirmation;
