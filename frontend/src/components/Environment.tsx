import React from 'react';
import { Grid } from '@material-ui/core';
import styled from 'styled-components';
import { Card, Elevation } from '@blueprintjs/core';
import Diagram from '../assets/devsecops_process.png';

const Img = styled.img`
  margin: 2rem;
  max-width: 80%;
  max-height: 80%;
`;

const Environment = () => {
  return (
    <Grid container direction={'column'}>
      <Grid item>
        <h1>Environment</h1>
        <p>The below diagram is whats created on the AWS infrastructure</p>
      </Grid>
      <Grid item>
        <Img src={Diagram} />
      </Grid>
      <Grid item>
        <Grid container spacing={10} direction={'column'} justify={'space-around'}>
          <Grid item>
            <Card elevation={Elevation.TWO}>
              <h2>AWS CodePipeline</h2>
              <p>
                AWS CodePipeline is a continuous delivery service you can use to model,
                visualize, and automate the steps required to release your software. You
                can quickly model and configure the different stages of a software release
                process. AWS CodePipeline automates the steps required to release your
                software changes continuously.{' '}
              </p>
            </Card>
          </Grid>
          <Grid item>
            <Card elevation={Elevation.TWO}>
              <h2>AWS S3</h2>
              <p>
                Amazon S3 is a cloud computing web service offered by Amazon Web Services.
                Amazon S3 provides object storage through web services interfaces.
              </p>
            </Card>
          </Grid>
          <Grid item>
            <Card elevation={Elevation.TWO}>
              <h2>AWS ECR</h2>
              <p>
                Amazon Elastic Container Registry (Amazon ECR) is a managed AWS Docker
                registry service that is secure, scalable, and reliable. Amazon ECR
                supports private Docker repositories with resource-based permissions using
                AWS IAM so that specific users or Amazon EC2 instances can access
                repositories and images. Developers can use the Docker CLI to push, pull,
                and manage images.{' '}
              </p>
            </Card>
          </Grid>
          <Grid item>
            <Card elevation={Elevation.TWO}>
              <h2>AWS Beanstalk</h2>
              <p>
                With Elastic Beanstalk, you can quickly deploy and manage applications in
                the AWS Cloud without worrying about the infrastructure that runs those
                applications. AWS Elastic Beanstalk reduces management complexity without
                restricting choice or control. You simply upload your application, and
                Elastic Beanstalk automatically handles the details of capacity
                provisioning, load balancing, scaling, and application health monitoring.{' '}
              </p>
            </Card>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Environment;
