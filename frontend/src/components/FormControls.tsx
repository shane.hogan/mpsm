import React  from 'react';
import {
  EditableText,
  FormGroup,
  InputGroup,
  Intent
} from '@blueprintjs/core';
import { WrappedFieldsProps } from 'redux-form';

interface InputProps {
  labelinfo?: string;
  helper?: string;
  name?: string;
}

export const renderTextInput: React.FunctionComponent<
  InputProps & WrappedFieldsProps
> = ({ input, label, name, labelinfo, meta: { touched, error}, ...custom }) => {
  return (
    <FormGroup
      label={label}
      labelFor={name}
      helperText={touched && error}
      intent={touched && error ? Intent.DANGER : Intent.NONE}
      labelInfo={labelinfo}
    >
      <InputGroup
        id={'test'}
        intent={touched && error ? Intent.DANGER : Intent.NONE}
        {...input}
        {...custom}
      />
    </FormGroup>
  );
};

export const renderEditableText: React.FunctionComponent<WrappedFieldsProps> = ({
  input,
  label,
  meta: { touched, error },
  ...custom
}) => {
  return <EditableText {...input} {...custom} />;
};

class Validations {
  required = (value: any) =>
    value || typeof value === 'number' ? undefined : 'Required';
  maxLength = (max: any) => (value: any) =>
    value && value.length > max ? `Must be ${max} characters or less` : undefined;
  minLength = (min: any) => (value: any) =>
    value && value.length < min ? `Must be ${min} characters or more` : undefined;
  number = (value: any) =>
    value && isNaN(Number(value)) ? 'Must be a number' : undefined;
  minValue = (min: any) => (value: any) =>
    value && value < min ? `Must be at least ${min}` : undefined;
  email = (value: any) =>
    value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
      ? 'Invalid email address'
      : undefined;
  alphaNumeric = (value: any) =>
    value && /[^a-zA-Z0-9 ]/i.test(value) ? 'Only alphanumeric characters' : undefined;
  phoneNumber = (value: any) =>
    value && !/^(0|[1-9][0-9]{9})$/i.test(value)
      ? 'Invalid phone number, must be 10 digits'
      : undefined;
}
export const Validator = new Validations();
