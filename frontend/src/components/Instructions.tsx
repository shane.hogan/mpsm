import React from 'react';
import { Grid } from '@material-ui/core';
import { Callout, Card, Elevation, Intent } from '@blueprintjs/core';
import styled from 'styled-components';

const BorderCard = styled(Card)`
  border-top: 5px solid ${props => (props.color === 'orange' ? 'orange' : '#2196f3')} !important;
`;

interface InstructionProps {
  title: string;
  number: number;
  location?: string;
}
const Instruction: React.FunctionComponent<InstructionProps> = ({
  title,
  number,
  children,
  location
}) => (
  <Grid item>
    <BorderCard elevation={Elevation.TWO} color={location === 'aws' ? 'orange' : 'blue'}>
      <h3>
        {number}. {title}
      </h3>
      <p>{children}</p>
    </BorderCard>
  </Grid>
);

const Instructions = () => {
  return (
    <Grid container justify={'center'} direction={'column'} spacing={10}>
      <Grid item>
        <Callout intent={Intent.WARNING} title={'AWS Account Needed'}>
          <p>
            In order to create the AWS Environment and deploy the application you will
            need an active AWS account.
            <br />
            If you do not already have an account, please follow the instructions{' '}
            <a
              href='https://portal.aws.amazon.com/billing/signup#/start'
              target={'_blank'}
            >
              here
            </a>{' '}
            on setting one up.
            <br />
            If you do already have an account, please login to the account now and then
            continue.
          </p>
        </Callout>
      </Grid>
      <Instruction number={1} title={`Set Region to "N.Virginia"`} location={'aws'}>
        Make sure the Region is set to "N.Virginia". The region can be set in the upper
        right hand corner to the left of 'Support'.
      </Instruction>
      <Instruction number={2} title={'Create New User Group'} location={'aws'}>
        - Under Services search for IAM
        <br />
        - Click Groups on left hand side
        <br />
        - Click Create New Group
        <br />
        - Enter Unique Group name then click Next Step on bottom right
        <br />- Select the check box next to <b>AdministratorAcccess</b> (should be top
        Policy Name) then click Next Step
        <br />
        - Click Create Group
        <br />
      </Instruction>
      <Instruction number={3} title={'Create New User'} location={'aws'}>
        - Under Services search for <b>IAM</b>
        <br />
        - Click Users on left hand side
        <br />
        - Click Add User on top left
        <br />
        - Enter a user name
        <br />- AccessType is <b>Programmatic Access</b>
        <br />
        - Select Add user to group box
        <br />
        - Select the checkbox next to the Group you just created then click Next
        <br />
        - Click Next: Review
        <br />- Click Create User - <b>DO NOT CLOSE THE NEXT PAGE</b> without first
        downloading the credentials file.
        <br />
        - Download the CSV credentials file using the Download .csv button
        <br />
        - This file has all the necessary information to deploy the entire application
        environment
        <br />
      </Instruction>
      <Instruction number={4} title={'Deploy Application'}>
        - Click 'New App' in top right
        <br />
        - Enter in an Application name
        <br />
        - Enter in the AWS Account Id
        <br />
        &nbsp;&nbsp;&nbsp;&nbsp;- This is the leading numbers in the URL in the Console
        login Link cell of the Credentials file
        <br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- ex:
        https://403066205450.signin.aws.amazon.com/console
        <br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- The Account id is 403066205450
        <br />
        - Enter in the AWS Access Key
        <br />
        - This is found in the 'Access key ID' column in the credentials file
        <br />
        - Enter in the Secret Access key
        <br />
        - This is found in the 'Secret Access Key' column in the credentials file
        <br />
        - Click Submit
        <br />
        - The entire building of the environment, pulling the application, building,
        testing, and deploying it can take 5-20 minutes.
        <br />
        - After submitting, you can check the status on the 'View Apps' page, by clicking
        the 'View Apps' button in the top right
        <br />
        - When the environment is setup the application status will be 'Running'
        <br />
        - However, it may take another few minutes for the application to actual be
        deployed and viewable at the URL
        <br />
      </Instruction>
    </Grid>
  );
};

export default Instructions;
