import React from 'react';
import { Button, Classes, FormGroup, InputGroup } from '@blueprintjs/core';
import * as _ from 'lodash';

interface LoadingFormProps {
  numberOfFields: number;
}

const LoadingForm: React.FunctionComponent<LoadingFormProps> = ({ numberOfFields }) => {
  return (
    <form>
      <h1 className={Classes.SKELETON}>Loading</h1>
      {_.range(numberOfFields).map((index: number) => (
        <FormGroup key={index} className={Classes.SKELETON}>
          <InputGroup className={Classes.SKELETON} />
        </FormGroup>
      ))}
      <Button className={Classes.SKELETON}>Submit</Button>
    </form>
  );
};

export default LoadingForm;
