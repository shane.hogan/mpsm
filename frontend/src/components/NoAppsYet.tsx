import React from 'react';
import { Button, Intent, NonIdealState } from '@blueprintjs/core';

interface NoAppsYetProps {
  action: () => void;
}

const NoAppsYet: React.FunctionComponent<NoAppsYetProps> = ({ action }) => {
  return (
    <NonIdealState
      icon={'console'}
      title='No Deployed Applications'
      description={'You have not deployed any applications yet.'}
      action={
        <Button intent={Intent.PRIMARY} onClick={() => action()}>
          Deploy One Now
        </Button>
      }
    />
  );
};

export default NoAppsYet;
