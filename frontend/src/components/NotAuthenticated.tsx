import React from 'react';
import { Button, Intent, NonIdealState } from '@blueprintjs/core';

interface NoAppsYetProps {
  action: () => void;
}

export const NotAuthenticated: React.FunctionComponent<NoAppsYetProps> = ({ action }) => {
  return (
    <NonIdealState
      icon={'blocked-person'}
      title='Access Denied.'
      description={'You are not signed in.'}
      action={
        <Button intent={Intent.PRIMARY} onClick={() => action()}>
          Sign In Now
        </Button>
      }
    />
  );
};

export default NotAuthenticated;
