import React from 'react';
import { Button, Divider, FileInput, FormGroup, H2, Intent } from '@blueprintjs/core';

interface OwnProps {
  onSubmit: (values: any) => void;
}
interface OwnState {
  filename: string;
  file?: File | null;
}
export default class SettingsFileFormCom extends React.Component<OwnProps, OwnState> {
  constructor(props: OwnProps) {
    super(props);
    this.state = {
      filename: 'Choose a file....',
      file: null
    };
  }

  handleChange = (e: any) => {
    const file = e.target.files[0];
    this.setState({ file: file, filename: file.name });
  };

  handleSubmit = (e: any) => {
    e.preventDefault();
    this.props.onSubmit({ credentials_file: this.state.file });
  };

  render() {
    const { filename } = this.state;
    return (
      <>
        <H2>Default AWS Settings</H2>
        <Divider />
        <form onSubmit={this.handleSubmit.bind(this)}>
          <FormGroup
            label={'AWS  Credentials File'}
            helperText={'Only a valid credentials CSV file is accepted.'}
            labelInfo={'(required)'}
          >
            <FileInput
              fill
              large
              onInputChange={this.handleChange.bind(this)}
              text={filename}
            />
          </FormGroup>
          <Button intent={Intent.PRIMARY} type={'submit'}>
            Upload
          </Button>
        </form>
      </>
    );
  }
}
