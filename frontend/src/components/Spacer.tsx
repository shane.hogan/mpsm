import React, { FunctionComponent } from 'react';
import styled from 'styled-components';

const Padding = styled.div`
  padding: 1.5rem;
`;

const Spacer: FunctionComponent = ({ children }) => {
  return <Padding>{children}</Padding>;
};

export default Spacer;
