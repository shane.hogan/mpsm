import React from 'react';
import {Grid} from "@material-ui/core";
import TerraformDiagram from '../assets/terraform_build.png';
import ReactSVG from "react-svg";
import styled from "styled-components";
const Img = styled.img`
  margin: 2rem;
  max-width: 80%;
  max-height: 80%;
`;
const Terraform = () => {
    return (
        <Grid container justify={'center'} direction={'column'} spacing={10}>
            <Grid item>
                <h1>How It's Made</h1>
                <p>clearAvenue uses <b>Terraform</b> to safely create, change, and scale it's infrastructure.</p>
            </Grid>
            <Grid item>
                <Grid container justify={"center"} alignItems={"center"} alignContent={"center"}>
                    <Grid item>
                        <Img src={TerraformDiagram}/>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
};

export default Terraform;
