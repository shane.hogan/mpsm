import React from 'react';
import ReactDOM from 'react-dom';
import '@blueprintjs/core/lib/css/blueprint.css';
import '@blueprintjs/icons/lib/css/blueprint-icons.css';
import {Provider} from 'react-redux';

import * as serviceWorker from './serviceWorker';
import './index.css';
import store from './redux/store';
import Auth from './Auth/Auth';
import App from './App';
import {Auth0Provider} from "./auth0-spa";

const clientId = process.env.REACT_APP_AUTH_CLIENT_ID || '';
const domain = process.env.REACT_APP_AUTH_DOMAIN || '';
const redirectUri = process.env.REACT_APP_AUTH_REDIRECT_URI || '';
const onRedirectCallback = (appState: any) => {
    window.history.replaceState(
        {},
        document.title,
        appState && appState.targetUrl
            ? appState.targetUrl
            : window.location.pathname
    );
};
const auth = new Auth();

ReactDOM.render(
  <Provider store={store}>
      <Auth0Provider
          domain={domain}
                 client_id={clientId}
                 redirect_uri={redirectUri}
          // @ts-ignore
                 onRedirectCallback={onRedirectCallback}
      >
    <App auth={auth} />
      </Auth0Provider>
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
