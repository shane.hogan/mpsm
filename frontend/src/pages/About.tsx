import React, { Component } from 'react';
import { Grid } from '@material-ui/core';
import {Tab, Tabs} from '@blueprintjs/core';

import Environment from '../components/Environment';
import Instructions from '../components/Instructions';
import Terraform from "../components/Terraform";

class About extends Component {
  render() {
    return (
      <Grid
        container
        style={{ width: '100%', height: '100%', padding: '1rem' }}
        justify={'center'}
      >
        <Tabs id={'about_tags'} animate={true}>
          <Tab id='env' title='Environment' panel={<Environment />} />
          <Tab id='how' title={`How It's Made`} panel={<Terraform/>}/>
          <Tab id={'gettingstarted'} title={'Getting Started'} panel={<Instructions />} />
        </Tabs>
      </Grid>
    );
  }
}

export default About;
