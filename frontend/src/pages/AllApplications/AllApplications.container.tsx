import { failed, setApplications, setLoading } from '../../redux/deployment.reducer';
import { GetCreatedApps } from '../../actions/DeploymentActions';
import { connect } from 'react-redux';
import AllApplications from './AllApplications';
import { RouteComponentProps } from 'react-router';
import { IApplicationModel } from '../../../../api/src/Models/ApplicationModel';

interface OwnProps extends RouteComponentProps {}

interface StateProps {
  apps: IApplicationModel[];
  loading: boolean;
}

interface DispatchProps {
  getAllApps: () => {};
}
export type ComponentProps = OwnProps & StateProps & DispatchProps;

const mapStateToProps = (state: any): StateProps => ({
  apps: state.applications.applications,
  loading: state.applications.isLoading
});
const mapDispatchToProps = (dispatch: any): DispatchProps => ({
  getAllApps: async () => {
    dispatch(setLoading(true));
    const result = await GetCreatedApps();
    if (result.ok) {
      dispatch(setApplications(result.data));
    } else {
      dispatch(failed(result.problem));
    }
  }
});

export default connect<StateProps, DispatchProps, OwnProps>(
  mapStateToProps,
  mapDispatchToProps
)(AllApplications);
