import React, { Component } from 'react';
import * as _ from 'lodash';
import ApplicationCard, { LoadingCard } from '../../components/ApplicationCard';
import { StopAndDeleteApp } from '../../actions/DeploymentActions';
import { Button } from '@blueprintjs/core';
import { Grid } from '@material-ui/core';
import styled from 'styled-components';
import { IApplicationModel } from '../../../../api/src/Models/ApplicationModel';
import DeleteAppConfirmation from '../../components/DeleteAppConfirmation';
import NoAppsYet from '../../components/NoAppsYet';
import { ComponentProps } from './AllApplications.container';

const Spacer = styled.div`
  padding: 2rem;
`;

interface AllApplicationsState {
  loading?: boolean;
  deleteOpen: boolean;
  deleteApp?: IApplicationModel;
}

export default class AllApplications extends Component<
  ComponentProps,
  AllApplicationsState
> {
  constructor(props: ComponentProps) {
    super(props);
    this.state = { deleteOpen: false };
    this.viewLogs = this.viewLogs.bind(this);
    this.stopAndDelete = this.stopAndDelete.bind(this);
  }

  componentDidMount() {
    this.props.getAllApps();
  }

  viewLogs = (id: string) => {
    this.props.history.push('/logs/' + id);
  };

  stopAndDelete = (id: string) => {
    const deleteApp = _.find(this.props.apps, { _id: id }) as IApplicationModel;
    this.setState({
      deleteApp: deleteApp,
      deleteOpen: true
    });
  };

  confirmStopAndDelete = async (id: string) => {
    this.setState({ deleteOpen: false, deleteApp: undefined });
    const result = await StopAndDeleteApp(id);
    if (result.ok) {
      this.props.getAllApps();
      this.setState({ deleteOpen: false, deleteApp: undefined });
    } else {
      alert('Failed to Stop and Delete. Please try again');
    }
  };

  cancelStopAndDelete = () => {
    this.setState({ deleteApp: undefined, deleteOpen: false });
  };

  renderZeroAppsPlaceholder = () => {
    const { history, apps, loading } = this.props;
    if (!loading && (!apps || apps.length === 0))
      return <NoAppsYet action={() => history.push('/create')} />;
  };

  renderSkeletons = (number: number) => {
    if (this.props.loading) {
      return _.range(number).map((index: number) => {
        return (
          <Grid item xs={12} md={3} key={index}>
            <LoadingCard />
          </Grid>
        );
      });
    }
  };

  renderApps = () => {
    const { loading, apps } = this.props;
    if (!loading && apps && apps.length > 0) {
      return apps.map((app: IApplicationModel) => {
        return (
          <Grid item xs={12} md={3} key={app.unique_name}>
            <ApplicationCard
              app={app as IApplicationModel}
              logs={this.viewLogs}
              stopAndDelete={this.stopAndDelete}
            />
          </Grid>
        );
      });
    }
  };

  render() {
    const { deleteOpen, deleteApp } = this.state;
    const { loading, apps } = this.props;
    return (
      <Spacer>
        <Grid container justify={'flex-end'}>
          <Grid item>
            <Button
              icon='refresh'
              onClick={() => this.props.getAllApps()}
              loading={loading}
            >
              Refresh
            </Button>
          </Grid>
        </Grid>
        <br />
        <Grid container spacing={10} justify={'space-around'}>
          {deleteOpen && deleteApp && (
            <DeleteAppConfirmation
              isOpen={deleteOpen}
              app={deleteApp}
              doConfirm={this.confirmStopAndDelete}
              doCancel={this.cancelStopAndDelete}
            />
          )}
          {this.renderApps()}
          {this.renderZeroAppsPlaceholder()}
          {this.renderSkeletons(apps.length || 8)}
        </Grid>
      </Spacer>
    );
  }
}
