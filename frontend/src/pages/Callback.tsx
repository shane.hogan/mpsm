import React  from 'react';
import loading from '../assets/cs-logo.svg';
import {Redirect} from "react-router";
import {useAuth0} from "../auth0-spa";


const Callback: React.FunctionComponent = () => {
  const {isAuthenticated} = useAuth0();
  return isAuthenticated ? <Redirect to={'/'}/> :  <img src={loading} alt='loading' />
};


export default Callback;
