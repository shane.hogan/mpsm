import CreateApplication from './CreateApplication';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import IProfile from '../../../../api/src/interfaces/IProfile';
import { IState } from '../../redux/store';
import { GetProfile } from '../../actions/ProfileActions';
import { setLoading as profileLoading, setProfile } from '../../redux/profile.reducer';
import { CreateNewApp } from '../../actions/DeploymentActions';
import { createAppSuccess, failed, setLoading } from '../../redux/deployment.reducer';

export interface OwnProps extends RouteComponentProps {
  profile?: IProfile;
}
interface StateProps {
  error?: string;
  profile?: IProfile;
  isLoading: boolean;
  initialValues?: IProfile;
  loadingProfile: boolean;
}

interface DispatchProps {
  loadProfile: () => void;
  createApplication: (formData: any) => void;
}
export type ComponentProps = OwnProps & StateProps & DispatchProps;

const mapStateToProps = (s: any): StateProps => {
  const state: IState = s as IState;
  return {
    error: state.applications.error,
    profile: state.profile.profile,
    loadingProfile: state.profile.isLoading,
    isLoading: state.applications.isLoading,
    initialValues: {
      ...state.profile.profile
    }
  };
};

const mapDispatchToProps = (dispatch: any): DispatchProps => ({
  loadProfile: async (): Promise<void> => {
    dispatch(profileLoading(true));
    let profileResult = await GetProfile();
    if (profileResult.ok) {
      dispatch(setProfile(profileResult.data));
    } else {
      dispatch(profileLoading(false));
    }
  },
  createApplication: async (formData: any): Promise<void> => {
    dispatch(setLoading(true));
    const formResult = await CreateNewApp(formData);
    if (formResult.ok) {
      dispatch(createAppSuccess({}));
    } else {
      console.log(formResult);
      if (formResult.data && formResult.data) {
        let error = formResult.data as { stack: string; message: string };
        dispatch(failed(error.message));
      }
    }
  }
});

export default connect<StateProps, DispatchProps, OwnProps>(
  mapStateToProps,
  mapDispatchToProps
)(CreateApplication);
