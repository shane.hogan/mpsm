import React, { Component, SyntheticEvent } from 'react';
import { Alert, Card, Elevation, H3, Intent } from '@blueprintjs/core';
import { Grid } from '@material-ui/core';

import Spacer from '../../components/Spacer';
import SimpleForm from '../../components/CreateApplicationForm';
import { ComponentProps } from './CreateApplication.container';
import LoadingForm from '../../components/LoadingForm';

interface OwnState {
  isAlertOpen: boolean;
}

class CreateApplication extends Component<ComponentProps, OwnState> {
  constructor(props: ComponentProps) {
    super(props);
    this.state = {
      isAlertOpen: false
    };
    this.submitSimpleForm = this.submitSimpleForm.bind(this);
  }

  componentDidMount(): void {
    const { loadProfile } = this.props;
    loadProfile();
  }

  componentDidUpdate(
    prevProps: Readonly<ComponentProps>,
    prevState: Readonly<any>,
    snapshot?: any
  ): void {
    const { isLoading: wasLoading } = prevProps;
    const { isLoading } = this.props;
    if (wasLoading && !isLoading) {
      this.setState({ isAlertOpen: true });
    }
  }

  alertConfirmed = (confirmed: boolean, evt?: SyntheticEvent<HTMLElement>) => {
    const { error } = this.props;
    this.setState({ isAlertOpen: false });
    if (!error) {
      this.props.history.push('/all');
    }
  };

  submitSimpleForm = (values: any) => {
    const { createApplication } = this.props;
    createApplication(values);
  };

  renderAlert() {
    const { isAlertOpen } = this.state;
    const { error } = this.props;
    return (
      <Alert
        isOpen={isAlertOpen}
        onClose={this.alertConfirmed}
        intent={error ? Intent.DANGER : Intent.SUCCESS}
      >
        <H3>{error ? 'Error!' : 'Success!'}</H3>
        <p>{error ? error : 'Application Created Successfully!'}</p>
      </Alert>
    );
  }

  render() {
    const { loadingProfile, initialValues } = this.props;
    return (
      <Spacer>
        {this.renderAlert()}
        <Grid container justify={'space-around'} alignItems={'center'}>
          <Grid item xs={12} md={6}>
            <Card interactive={true} elevation={Elevation.TWO}>
              {!loadingProfile && (
                <SimpleForm
                  onSubmit={this.submitSimpleForm}
                  initialValues={initialValues}
                />
              )}
              {loadingProfile && <LoadingForm numberOfFields={7} />}
            </Card>
          </Grid>
        </Grid>
      </Spacer>
    );
  }
}

export default CreateApplication;
