import React, { Component } from 'react';
import { NotAuthenticated as ErrMsg } from '../components/NotAuthenticated';
import Auth from '../Auth/Auth';
import { Redirect, RouteComponentProps, withRouter } from 'react-router';
import { connect } from 'react-redux';

interface RouteParams {
  desiredPath: string;
}
interface NotAuthenticatedProps extends RouteComponentProps<RouteParams> {
  auth: Auth;
  loginWithRedirect: () => {};
}
interface StateProps {
  auth0: any;
}
type ComponentProps = NotAuthenticatedProps & StateProps;
class NotAuthenticated extends Component<ComponentProps, { redirect: boolean }> {
  constructor(props: ComponentProps) {
    super(props);
    this.state = {
      redirect: false
    };
  }

  componentDidUpdate(
    prevProps: Readonly<ComponentProps>,
    prevState: Readonly<{}>,
    snapshot?: any
  ): void {
    const { auth0: prevAuth } = prevProps;
    const { auth0 } = this.props;
    if (prevAuth === null && auth0) {
      this.setState({ redirect: true });
    }
  }

  render() {
    const {
      match,
      loginWithRedirect
    } = this.props;
    const { redirect } = this.state;
    if (redirect) {
      return <Redirect to={'/' + match.params.desiredPath} />;
    }
    return <ErrMsg action={() => loginWithRedirect()} />;
  }
}

const mapStateToProps = (state: any) => ({
  auth0: state.auth0
});
export default connect(mapStateToProps)(withRouter(NotAuthenticated));
