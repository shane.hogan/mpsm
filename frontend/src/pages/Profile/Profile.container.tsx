import Profile from './Profile';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import IProfile from '../../../../api/src/interfaces/IProfile';
import { IState } from '../../redux/store';
import { error, setLoading, setProfile } from '../../redux/profile.reducer';
import {
  DeleteProfile,
  GetProfile,
  UpdateProfile,
  UploadCredentialsFile
} from '../../actions/ProfileActions';

interface StateProps {
  profile: IProfile;
  initialValues: IProfile;
  isLoading: boolean;
  error?: string;
}

interface DispatchProps {
  getProfile: () => {};
  updateProfile: (profile: any) => {};
  uploadCredentialsFile: (file: File) => {};
  deleteProfile: () => {};
}
interface OwnProps extends RouteComponentProps {}

export type ComponentProps = StateProps & DispatchProps & OwnProps;

const mapStateToProps = (s: any): StateProps => {
  let state: IState = s as IState;
  return {
    profile: state.profile.profile,
    initialValues: {
      ...state.profile.profile
    },
    isLoading: state.profile.isLoading,
    error: state.profile.error
  };
};
const mapDispatchToProps = (dispatch: any): DispatchProps => ({
  getProfile: async (): Promise<void> => {
    dispatch(setLoading(true));
    const getProfileResult = await GetProfile();
    if (getProfileResult.ok) {
      dispatch(setProfile(getProfileResult.data as IProfile));
    } else {
      dispatch(error);
    }
  },
  updateProfile: async (profile: any): Promise<void> => {
    dispatch(setLoading(true));
    const updateProfileResult = await UpdateProfile(profile);
    if (updateProfileResult.ok) {
      dispatch(setProfile(updateProfileResult.data));
    } else {
      const updateError = updateProfileResult.data as { stack: string; message: string };
      dispatch(error(updateError.message));
    }
  },
  uploadCredentialsFile: async (file: File): Promise<void> => {
    dispatch(setLoading(true));
    const uploadFileResult = await UploadCredentialsFile(file);
    if (uploadFileResult.ok) {
      dispatch(setProfile(uploadFileResult.data));
    } else {
      const uploadError = uploadFileResult.data as { stack: string; message: string };
      dispatch(error(uploadError.message));
    }
  },
  deleteProfile: async () => {
    dispatch(setLoading(true));
    const deleteResult = await DeleteProfile();
    if (deleteResult.ok) {
      dispatch(setProfile(deleteResult.data));
    } else {
      const uploadError = deleteResult.data as { stack: string; message: string };
      dispatch(error(uploadError.message));
    }
  }
});

export default connect<StateProps, DispatchProps, OwnProps>(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
