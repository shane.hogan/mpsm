import React, { Component } from 'react';
import { Grid } from '@material-ui/core';
import { Alert, Button, Card, Elevation, H3, Intent } from '@blueprintjs/core';
import CardFlip from 'react-card-flip';
import Spacer from '../../components/Spacer';
import DefaultSettingsForm from '../../components/DefaultSettingsForm';
import SettingsFileFormCom from '../../components/SettingsFileForm';

import { ComponentProps } from './Profile.container';
import LoadingForm from '../../components/LoadingForm';

interface OwnState {
  isAlertOpen: boolean;
  updating: boolean;
  isFileForm: boolean;
}

class Profile extends Component<ComponentProps, OwnState> {
  constructor(props: ComponentProps) {
    super(props);
    this.state = {
      isAlertOpen: false,
      updating: false,
      isFileForm: true
    };
  }

  componentDidMount(): void {
    this.props.getProfile();
  }

  componentDidUpdate(
    prevProps: Readonly<ComponentProps>,
    prevState: Readonly<any>,
    snapshot?: any
  ): void {
    const { isLoading: wasLoading } = prevProps;
    const { isLoading } = this.props;
    const { updating } = this.state;
    if (wasLoading && !isLoading && updating) {
      this.setState({ isAlertOpen: true });
    }
  }

  handleSubmit = (values: any): void => {
    this.setState({ updating: true });
    this.props.updateProfile(values);
  };
  handleFileSubmit = (values: any): void => {
    this.setState({ updating: true });
    this.props.uploadCredentialsFile(values.credentials_file);
  };
  deleteProfile = () => {
    this.setState({ updating: true });
    this.props.deleteProfile();
  };

  renderAlert() {
    const { isAlertOpen } = this.state;
    const { error } = this.props;
    return (
      <Alert
        isOpen={isAlertOpen}
        onClose={() => this.setState({ isAlertOpen: false, updating: false })}
        intent={error ? Intent.DANGER : Intent.SUCCESS}
      >
        <H3>{error ? 'Error!' : 'Success!'}</H3>
        <p>{error ? error : 'Profile Updated Successfully!'}</p>
      </Alert>
    );
  }

  render() {
    const { isLoading, initialValues } = this.props;
    const { isFileForm } = this.state;
    return (
      <Spacer>
        {this.renderAlert()}
        <Grid
          container
          justify={'space-around'}
          direction={'column'}
          alignItems={'stretch'}
        >
          <Grid item>{/*<H1>Shane Hogan</H1>*/}</Grid>
        </Grid>
        <Grid container justify={'space-around'} alignItems={'center'}>
          <Grid item xs={12}>
            <Grid container justify={'space-around'} alignItems={'center'}>
              <Grid item xs={12} md={6}>
                <Grid container justify={'flex-end'}>
                  <Button
                    intent={Intent.DANGER}
                    alignText={'right'}
                    onClick={() => this.deleteProfile()}
                    icon={'trash'}
                  >
                    Clear Profile Settings
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={12} md={6}>
            {!isLoading && (
              <CardFlip isFlipped={isFileForm}>
                <Card elevation={Elevation.TWO} key={'front'}>
                  <DefaultSettingsForm
                    onSubmit={this.handleSubmit}
                    initialValues={initialValues}
                  />
                  <br />
                    <Button minimal  onClick={() => this.setState({ isFileForm: !isFileForm })} >
                        Would you rather upload a single file?
                    </Button>
                </Card>

                <Card elevation={Elevation.TWO} key={'back'}>
                  <SettingsFileFormCom key='back' onSubmit={this.handleFileSubmit} />
                  <br />
                    <Button minimal onClick={() => this.setState({ isFileForm: !isFileForm })} >
                        Want to manually enter keys and IDs instead?
                    </Button>
                </Card>
              </CardFlip>
            )}
            {isLoading && (
              <Card elevation={Elevation.TWO}>
                <LoadingForm numberOfFields={4} />
              </Card>
            )}
          </Grid>
        </Grid>
      </Spacer>
    );
  }
}
export default Profile;
