import React, { Component } from 'react';
import { GetLogs } from '../actions/DeploymentActions';
import Spacer from '../components/Spacer';
import { Grid } from '@material-ui/core';
import {Button, Card, Classes, Elevation} from '@blueprintjs/core';
import { RouteComponentProps } from 'react-router';
import styled from 'styled-components';

const Code = styled.pre`
  white-space: pre-wrap;
`;

interface LogState {
  logs: string;
  loading: boolean;
}

interface RouteProps {
  id: string;
}

class ViewLogFile extends Component<RouteComponentProps<RouteProps>, LogState> {
  constructor(props: RouteComponentProps<RouteProps>) {
    super(props);
    this.state = {
      logs: '',
      loading: false
    };
  }

  async componentDidMount() {
    const { match } = this.props;
    const id = match.params.id;
    this.setState({ loading: true });
    const result = await GetLogs(id);
    this.setState({ loading: false });
    if (result.ok) {
      this.setState({ logs: result.data as string });
    } else {
      alert(result.problem);
    }
  }
  render() {
    const { loading, logs } = this.state;
    return (
      <Spacer>
        <Grid container justify={'space-around'} alignItems={'center'} spacing={8}>
          <Grid item xs={12}>
            <ul className='bp3-breadcrumbs'>
              <li>
                <Button minimal
                  className='bp3-breadcrumbs'
                  onClick={() => this.props.history.push('/all')}
                >
                  Applications
                </Button>
              </li>
              <li>
                <span className='bp3-breadcrumb bp3-breadcrumb-current'>Log File</span>
              </li>
            </ul>
          </Grid>
          <Grid item xs={12}>
            <Card interactive={true} elevation={Elevation.TWO}>
              <Code
                className={
                  loading ? Classes.SKELETON : 'bp3-monospace-text bp3-running-text'
                }
              >
                {logs}
              </Code>
            </Card>
          </Grid>
        </Grid>
      </Spacer>
    );
  }
}

export default ViewLogFile;
