import { createSlice } from 'redux-starter-kit';

const auth0 = createSlice({
  slice: 'auth0',
  initialState: null,
  reducers: {
    setAuth0Profile(state: any, action: any) {
      return (state = action.payload);
    },
    clearAuth0Profile(state: any, action: any) {
      return (state = null);
    }
  }
});

console.log(auth0);
const { actions, reducer } = auth0;
export const { setAuth0Profile, clearAuth0Profile } = actions;
export default reducer;
