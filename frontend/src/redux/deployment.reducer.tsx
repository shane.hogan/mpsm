import { createSlice } from 'redux-starter-kit';
import { IApplicationModel } from '../../../api/src/Models/ApplicationModel';

export interface IDeploymentsState {
  isLoading: boolean;
  applications: IApplicationModel[];
  error?: string;
}

const deployments = createSlice<IDeploymentsState, any>({
  slice: 'deployments',
  initialState: {
    isLoading: false,
    applications: []
  },
  reducers: {
    setLoading(state: IDeploymentsState, action: any) {
      state.isLoading = action.payload;
      return state;
    },
    createAppSuccess(state: IDeploymentsState, action: any) {
      state.isLoading = false;
      state.error = undefined;
      return state;
    },
    setApplications(state: IDeploymentsState, action: any) {
      state.isLoading = false;
      state.applications = action.payload;
      return state;
    },
    clearApplications(state: IDeploymentsState, action: any) {
      state.isLoading = false;
      state.applications = [];
      return state;
    },
    failed(state: IDeploymentsState, action: any) {
      state.isLoading = false;
      state.error = action.payload;
      return state;
    }
  }
});

const { actions, reducer } = deployments;
export const {
  setApplications,
  clearApplications,
  failed,
  setLoading,
  createAppSuccess
} = actions;
export default reducer;
