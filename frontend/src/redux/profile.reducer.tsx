import { createSlice, PayloadAction } from 'redux-starter-kit';
import IProfile from '../../../api/src/interfaces/IProfile';

export interface IProfileState {
  isLoading: boolean;
  profile: any | IProfile | null;
  error?: string;
}
const profile = createSlice<IProfileState, any>({
  slice: 'profile',
  initialState: {
    isLoading: false,
    profile: null
  },
  reducers: {
    setLoading(state: IProfileState, action: any) {
      state.error = undefined;
      state.isLoading = action.payload;
    },
    setProfile(state: IProfileState, action: PayloadAction<IProfile | null, string>) {
      state.isLoading = false;
      state.profile = action.payload;
      return state;
    },
    clearProfile(state: IProfileState, action: any) {
      state.isLoading = false;
      state.profile = null;
      return state;
    },
    error(state: IProfileState, action: any) {
      state.isLoading = false;
      state.error = action.payload;
      return state;
    }
  }
});

const { actions, reducer } = profile;
export const { setProfile, clearProfile, setLoading, error } = actions;
export default reducer;
