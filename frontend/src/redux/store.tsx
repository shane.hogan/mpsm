import {
  Action,
  AnyAction,
  configureStore,
  getDefaultMiddleware,
  Reducer
} from 'redux-starter-kit';
import { reducer as reduxFormReducer } from 'redux-form';
import auth0Reducer from './auth0.reducer';
import profileReducer, { IProfileState } from './profile.reducer';
import deploymentsReducer, { IDeploymentsState } from './deployment.reducer';

export interface IState {
  auth0: any;
  profile: IProfileState;
  applications: IDeploymentsState;
  form: any; // Not sure why FormReducer doesn't work here
}

const store = configureStore<IState, Action<any>>({
  reducer: {
    auth0: auth0Reducer as Reducer<null, AnyAction>,
    profile: profileReducer as Reducer<IProfileState, AnyAction>,
    applications: deploymentsReducer as Reducer<IDeploymentsState, AnyAction>,
    form: reduxFormReducer
  },
  middleware: [...getDefaultMiddleware()],
  devTools: process.env.NODE_ENV !== 'production'
});

export default store;
